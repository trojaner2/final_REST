package ch.fhnw.masam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServiceApplication {

	private static final Logger LOG1 = LogManager.getLogger(CovidController.class);
	
    public static void main(String[] args) {
    	LOG1.debug("Grüsse von den Trojanern");
        SpringApplication.run(RestServiceApplication.class, args);
    }

}

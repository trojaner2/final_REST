package ch.fhnw.masam;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CovidRepository extends JpaRepository<CovidCase, Integer> {

	@Query(value="SELECT * FROM tcovid_cases t where t.Country_Region = :country", nativeQuery=true)
	public List<CovidCase> findByCountry(String country);
	
	@Query(value="SELECT * FROM tcovid_cases t where t.date = :date", nativeQuery=true)
	public List<CovidCase> findByDate(String date);
	
	@Query(value="SELECT distinct country_region FROM tcovid_cases", nativeQuery=true)
	public List<String> getAllCountries();
	
}
